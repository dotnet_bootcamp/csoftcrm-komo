﻿using Domain.AggregatesModel.TeamAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations.TeamEntityConfiguration
{
    public class TeamEntityTypeConfiguration : IEntityTypeConfiguration<TeamEntity>
    {
        public void Configure(EntityTypeBuilder<TeamEntity> configuration)
        {
            configuration.ToTable("teams");
            configuration.HasKey(x => x.Id);
            configuration.Property(o => o.Id).HasColumnName("id");
            configuration.Property(o => o.Name).IsRequired().HasColumnName("Name");
            configuration.HasIndex(o => o.Name).IsUnique();
        }
    }
}