﻿using Domain.AggregatesModel.EmployeeAggregate;
using Domain.Exceptions;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using MediatR;
using UserDetails.Commands;
using Domain.AggregatesModel.RoleAggregate;
using SharedKernel.Domain.Helper;

namespace UserDetails.Handlers
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;

        public UpdateUserCommandHandler(IUserRepository userRepository, IUserQueries userQueries)
        {
            _userRepository = userRepository;
            _userQueries = userQueries;
        }

        public async Task<bool> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            User? user = await _userQueries
                .GetUserEntityAsync(request.Id)
                ?? throw new DomainException($"User with id : {request.Id} could not be found.");

            if (request.Email != user.Email)
            {
                User? existingUser = await _userQueries.FindByEmailAsync(request.Email);
                if (existingUser != null)
                {
                    throw new DomainException($"Email '{request.Email}' is already taken.");
                }
            }

            if (!EmailHelper.IsValidEmail(request.Email))
            {
                throw new DomainException($"{request.Email} is not valid email. Domain must be crocusoft.com");
            }

            if (request.RoleId == RoleParameter.SuperAdmin.Id)
            {
                throw new DomainException("You have attempted to set the user as Super Admin! Do not attempt this again! Your changes have not been saved.");
            }

            user.SetRole(request.RoleId);
            user.SetTeam(request.TeamId);
            user.SetDetails(request.Email.Trim(), request.FirstName, request.LastName);

            _userRepository
                .UpdateAsync(user);

            await _userRepository
                .UnitOfWork
                .SaveChangesAsync(cancellationToken);

            return true;
        }

        public class UpdateUserIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateUserCommand, bool>
        {
            public UpdateUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}