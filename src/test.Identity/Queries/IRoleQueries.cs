﻿using Identity.ViewModels;
using Domain.AggregatesModel.RoleAggregate;
using SharedKernel.Infrastructure.Queries;

namespace Identity.Queries
{
    public interface IRoleQueries : IQuery
    {
        Task<IEnumerable<RoleDto>> GetAllAsync();
        Task<Role> GetByNameAsync(RoleParameter role);
    }
}