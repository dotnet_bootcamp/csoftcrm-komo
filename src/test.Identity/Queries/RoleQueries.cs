﻿using AutoMapper;
using Domain.AggregatesModel.RoleAggregate;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Identity.ViewModels;
using Identity.Auth;

namespace Identity.Queries
{
    public class RoleQueries : IRoleQueries
    {
        private readonly AppDBContext _context;
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public RoleQueries(AppDBContext context, IUserManager manager, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _userManager = manager;
        }

        public async Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            var self = await _userManager.GetCurrentUser();
            List<Role>? entities;
            if(self.RoleId == RoleParameter.SuperAdmin.Id)
            {
				entities = await _context.Roles.ToListAsync();
				return _mapper.Map<IEnumerable<RoleDto>>(entities);
			}
            entities  = await _context.Roles
                .Where(r => r.Id != RoleParameter.SuperAdmin.Id)
                .ToListAsync();

            return _mapper.Map<IEnumerable<RoleDto>>(entities);
        }

        public async Task<Role> GetByNameAsync(RoleParameter role)
        {
            var entity = await _context.Roles.FirstOrDefaultAsync(p => p.Name == role.Name);

            return entity;
        }
    }
}
