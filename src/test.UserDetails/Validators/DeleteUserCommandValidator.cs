﻿using FluentValidation;
using UserDetails.Commands;

namespace UserDetails.Validators
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
        }
    }
}
