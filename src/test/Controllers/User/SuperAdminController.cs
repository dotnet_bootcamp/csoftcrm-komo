﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using CustomAuthorizeLogic;
using Extensions;
using SharedKernel.Domain.Seedwork;
using UserDetails.Commands;
using UserDetails.Commands;

namespace Presentation.Controllers.User
{
    [Route("api/superadmin")]
    [ApiController]
    public class SuperAdminController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SuperAdminController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [AuthorizeRoles(CustomRole.SuperAdmin)]
        [HttpPatch("changestatus")]
        public async Task<IActionResult> ChangeStatus([FromBody] ActiveUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ActiveUserCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin)]
        [HttpPatch("resetpassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ResetPasswordCommand,bool>(command,requestId);
            return NoContent();
        }
    }
}
