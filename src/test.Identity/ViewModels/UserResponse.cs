﻿namespace Identity.ViewModels
{
    public class UserResponse
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; private set; }
        private readonly List<UserProjectResponse> _userProject;
        public IReadOnlyCollection<UserProjectResponse> UserProjects => _userProject;
        public RoleDto Role { get; set; }
        public TeamResponse Team { get; set; }
        public bool IsActive { get; private set; }

        public UserResponse()
        {
            _userProject = new List<UserProjectResponse>();
        }
    }
}
