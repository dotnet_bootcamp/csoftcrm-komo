﻿using Domain.AggregatesModel.ProjectAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations.ProjectEntityConfiguration
{
    public class ProjectEntityTypeConfiguration : IEntityTypeConfiguration<ProjectEntity>
    {
        public void Configure(EntityTypeBuilder<ProjectEntity> configuration)
        {
            configuration.ToTable("projects");

            configuration.HasKey(o => o.Id);
            configuration.Property(o => o.Id).HasColumnName("id");

        }
    }
}
