﻿namespace Infrastructure.Constants
{
    public class LoadMoreDto
    {
        public int? Skip { get; set; } = 0;
        public int? Take { get; set; } = 5;
        public string? SortField { get; set; }
        public bool OrderBy { get; set; } = true;
    }
}