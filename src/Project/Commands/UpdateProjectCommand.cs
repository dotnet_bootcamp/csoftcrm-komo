﻿using MediatR;
using Project.DTO;

namespace Project.Commands
{
    public class UpdateProjectCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public UpdateProjectUserIdDTO? UserDTO { get; set; } 
    }
}
