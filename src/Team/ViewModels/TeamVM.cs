﻿using Identity.ViewModels;

namespace Team.ViewModels
{
    public class TeamVM
    {
        public int Id { get; set; }
        public string Name { get; set; }

        private readonly List<UserProfileDto> _employees;
        public IReadOnlyCollection<UserProfileDto> _teamMembers => _employees;

        public TeamVM()
        {
            _employees = new List<UserProfileDto>();
        }
    }
}
