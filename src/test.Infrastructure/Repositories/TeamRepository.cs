﻿using Domain.AggregatesModel.TeamAggregate;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Database;
using SharedKernel.Infrastructure;

namespace Infrastructure.Repositories
{
    public class TeamRepository : Repository<TeamEntity>, ITeamRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public TeamRepository(AppDBContext context)
        {
            Context = context;
        }
    }
}
