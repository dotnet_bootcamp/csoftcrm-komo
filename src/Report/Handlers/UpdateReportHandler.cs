﻿using Domain.AggregatesModel.DailyReportAggregate;
using Identity.Auth;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using MediatR;
using Report.Commands;

namespace Report.Handlers
{
    public class UpdateReportHandler : IRequestHandler<UpdateReportCommand, bool>
    {
        private readonly IReportRepository _repository;
        private readonly IUserManager _userManager;
        public UpdateReportHandler(IReportRepository repository, IUserManager userManager)
        {
            _repository = repository;
            _userManager = userManager;
        }

        public async Task<bool> Handle(UpdateReportCommand request, CancellationToken cancellationToken)
        {
            DailyReport? data = await _repository.GetAsync(request.Id);
            int? selfId = _userManager.GetCurrentUserId();

            if (data.created_by_id != selfId)
            {
                throw new UnauthorizedAccessException("You are not authorized to update this project.");
            }
            
            if (data.record_date.Date != DateTime.UtcNow.Date)
            {
                throw new UnauthorizedAccessException($"Access to update this report has passed.");
            }

            data.SetDetails(request.Summary,request.ProjectId);
            _repository.UpdateAsync(data);
            await _repository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class RegisterUpdateReportIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateReportCommand, bool>
    {
        public RegisterUpdateReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}