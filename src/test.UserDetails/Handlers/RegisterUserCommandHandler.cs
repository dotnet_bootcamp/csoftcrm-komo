﻿using MediatR;
using Domain.AggregatesModel.EmployeeAggregate;
using Domain.Exceptions;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using SharedKernel.Infrastructure;
using UserDetails.Commands;
using SharedKernel.Domain.Helper;
using Domain.AggregatesModel.RoleAggregate;
using Identity.Auth;

namespace UserDetails.Handlers
{
    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;
        private readonly IUserManager _userManager;

        public RegisterUserCommandHandler(IUserRepository userRepository, IUserQueries userQueries, IUserManager userManager)
        {
            _userRepository = userRepository;
            _userQueries = userQueries;
            _userManager = userManager;
        }

        public async Task<bool> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var self = await _userManager.GetCurrentUser();
            if (request.RoleId == RoleParameter.SuperAdmin.Id)
            {
                throw new UnauthorizedAccessException("You are not allowed to create superadmin accounts.");
            }

            if (self.RoleId != RoleParameter.SuperAdmin.Id && request.RoleId == RoleParameter.Admin.Id)
            {
                throw new UnauthorizedAccessException("You are not allowed to create administrative accounts.");
            }

            if (!EmailHelper.IsValidEmail(request.Email))
            {
                throw new DomainException($"{request.Email} is not valid email. Domain must be crocusoft.com");
            }

            if (!request.Password.Equals(request.ConfirmPassword))
            {
                throw new DomainException("Password do not match. Check and try again.");
            }

            User existingUser = await _userQueries
                .FindByEmailAsync(request.Email);

            if (existingUser != null)
            {
                throw new DomainException($"User name '{request.Email}' already taken, please choose another name.");
            }

            User user = new(request.FirstName, request.LastName, request.Email.Trim(), PasswordHasher.HashPassword(request.Password));
            user.SetRole(request.RoleId);
            user.SetTeam(request.TeamId);

            await _userRepository.AddAsync(user);
            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            return true;
        }
    }

    public class RegisterUserIdentifiedCommandHandler :
        IdentifiedCommandHandler<RegisterUserCommand, bool>
    {
        public RegisterUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; 
        }
    }
}
