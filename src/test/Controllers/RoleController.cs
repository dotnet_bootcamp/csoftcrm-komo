﻿using CustomAuthorizeLogic;
using Identity.Queries;
using Identity.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SharedKernel.Domain.Seedwork;

namespace Controllers
{
    [Route("role")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleQueries _roleQueries;

        public RoleController(IRoleQueries roleQueries)
        {
            _roleQueries = roleQueries ?? throw new ArgumentNullException(nameof(roleQueries));
        }

        [HttpGet]
		[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
		public async Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            return await _roleQueries.GetAllAsync();
        }
    }
}
