﻿using MediatR;

namespace Report.Commands
{
    public class CreateReportCommand : IRequest<bool>
    {
        public string Summary { get; set; }
        public int ProjectId { get; set; }

        public CreateReportCommand( string summary, int projectId)
        {
            Summary = summary;
            ProjectId = projectId;
        }
    }
}
