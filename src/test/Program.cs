using Microsoft.AspNetCore;
using Microsoft.Extensions.Options;
using Serilog;
using Extensions;
using Infrastructure;
using Infrastructure.Database;
using Infrastructure.DBSeed;

namespace test
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            Log.Logger = new LoggerConfiguration()
            .WriteTo.File("logs/log.txt", rollingInterval: RollingInterval.Day)
            .WriteTo.Seq("http://localhost:5341")
            .CreateLogger();

            host.MigrateDbContext<AppDBContext>(async (context, services) =>
            {
                var env = services.GetService<IWebHostEnvironment>();
                var settings = services.GetService<IOptions<CRMSettings>>();
                var logger = services.GetService<ILogger<DbContextSeed>>();

                var seeder = new DbContextSeed();
                await seeder.SeedAsync(context, env, settings, logger);
            });

            host.Run();
        }

        public static IWebHostBuilder CreateHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args).ConfigureServices((hostContext, services) =>
        {
          
        }).UseSerilog().UseStartup<Startup>();
    }
}
