﻿using Domain.AggregatesModel.DailyReportAggregate;
using Domain.AggregatesModel.ProjectAggregate;
using Domain.Exceptions;
using Identity.Auth;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Report.Commands;

namespace Report.Handlers
{
    public class CreateReportHandler : IRequestHandler<CreateReportCommand, bool>
    {
        private readonly IUserManager _userManager;
        private readonly IReportRepository _reportRepository;
        private readonly AppDBContext _context;

        public CreateReportHandler(IUserManager userManager, IReportRepository reportRepository, AppDBContext context)
        {
            _userManager = userManager;
            _reportRepository = reportRepository;
            _context = context;
        }

        public async Task<bool> Handle(CreateReportCommand request, CancellationToken cancellationToken)
        {
            int self = _userManager.GetCurrentUserId();

            UserProject? userProject = await _context
                .UserProjects
                .FirstOrDefaultAsync(up => up.UserId == self && up.ProjectId == request.ProjectId, cancellationToken: cancellationToken)
                ?? throw new DomainException("User is not associated with this project.");

            DailyReport newReport = new();
            newReport.SetDetails(request.Summary, request.ProjectId);

            newReport.SetAuditFields(self);
            await _reportRepository.AddAsync(newReport);
            await _reportRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }

        public class RegisterCreateReportIdentifiedCommandHandler : IdentifiedCommandHandler<CreateReportCommand, bool>
        {
            public RegisterCreateReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}
