﻿using Domain.Exceptions;
using MediatR;
using Report.Commands;
using Report.Queries;
using Report.Response;

namespace Report.Handlers
{
    public class ExportExcelQueryHandler : IRequestHandler<ExportExcelQuery, FileResponse>
    {
        private readonly IDailyReportQuery _query;
        public ExportExcelQueryHandler(IDailyReportQuery query)
        {
            _query = query;
        }

        public async Task<FileResponse> Handle(ExportExcelQuery request, CancellationToken cancellationToken)
        {
            var reports = await _query.GetDailyReportFilter(request.ReportFilter);

            if (reports.TotalCount == 0)
                throw new DomainException("No reports found. Export failed.");

            MemoryStream memoryStream = new(ExportToExcelService.ExportToExcel(reports.Data.ToList()));

            return new FileResponse
            {
                FileStream = memoryStream,
                ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                FileName = $"{DateTime.UtcNow} Report"
            };
        }
    }
}