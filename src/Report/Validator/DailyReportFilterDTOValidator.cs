﻿using FluentValidation;
using Report.DTO;

namespace Report.Validator
{
    public class DailyReportFilterDTOValidator : AbstractValidator<DailyReportFilterDTO>
    {
        public DailyReportFilterDTOValidator() : base()
        {
            RuleFor(command => command.LoadMore.Take).NotNull();
        }
    }
}