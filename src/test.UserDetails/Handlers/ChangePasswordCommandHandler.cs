﻿using Domain.AggregatesModel.EmployeeAggregate;
using Identity.Auth;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using SharedKernel.Infrastructure;
using MediatR;
using UserDetails.Commands;

namespace UserDetails.Handlers
{
    public class ChangePasswordCommandHandler : IRequestHandler<ChangePasswordCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserManager _userManager;

        public ChangePasswordCommandHandler(IUserRepository userRepository, IUserManager userManager)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task<bool> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            User? user = await _userManager.GetCurrentUser();
            string? oldPasswordHash = PasswordHasher.HashPassword(request.OldPassword);
            string newPasswordHash = PasswordHasher.HashPassword(request.NewPassword);
            user.SetPasswordHash(oldPasswordHash, newPasswordHash);
            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }
    }

    public class ChangePasswordIdentifiedCommandHandler : IdentifiedCommandHandler<ChangePasswordCommand, bool>
    {
        public ChangePasswordIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
