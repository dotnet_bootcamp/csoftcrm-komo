﻿using Domain.AggregatesModel.DailyReportAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations.DailyReportEntityConfiguration
{
    public class DailyReportEntityConfiguration : IEntityTypeConfiguration<DailyReport>
    {
        public void Configure(EntityTypeBuilder<DailyReport> configuration)
        {
            configuration.ToTable("daily_reports");

            configuration.HasKey(x => x.Id);
            configuration.Property(o => o.Id).HasColumnName("id");

            configuration.Property(o => o.Summary).HasMaxLength(1000).IsRequired().HasColumnName("summary");

            configuration.Property(o => o.record_date).IsRequired().HasColumnName("created_date");
            configuration.Property(o => o.last_update_date).HasColumnName("update_date");

            //Ignore
            configuration.Ignore(o => o.updated_by_id);
            configuration.Ignore(o => o.updated_by);

            configuration
                .HasOne(o => o.created_by)
                .WithMany(u => u.Reports)
                .HasForeignKey(o => o.created_by_id);

            configuration
                .HasOne(o => o.Project)
                .WithMany(p => p.UserReports)
                .HasForeignKey(o => o.ProjectId);
        }
    }
}