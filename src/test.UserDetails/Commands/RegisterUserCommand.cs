﻿using MediatR;

namespace UserDetails.Commands
{
    public class RegisterUserCommand : IRequest<bool>
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int RoleId { get; set; }
        public int? TeamId { get; set; }
        public List<int>? ProjectId { get; set; }
    }
}
