﻿using Domain.AggregatesModel.EmployeeAggregate;

namespace Identity.Auth
{
    public interface IUserManager
    {
        int GetCurrentUserId();

        string GetCurrentUserName();

        Task<User> GetCurrentUser();

        (string token, DateTime expiresAt) GenerateJwtToken(User user);
    }
}
