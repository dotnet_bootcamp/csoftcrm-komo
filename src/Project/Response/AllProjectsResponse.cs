﻿namespace Project.Response
{
    public class AllProjectsResponse
    {
        public IEnumerable<ProjectAllProjectsResponse> Data { get; set; }
        public int TotalCount { get; set; }
    }
}