﻿using Domain.AggregatesModel.ProjectAggregate;

namespace Project.Response
{
    public class GetProjectByIdResponse
    {
        public string Title { get; set; }
        private readonly List<UserProjectResponse> _employees;
        public IReadOnlyCollection<UserProjectResponse> UserProjects => _employees;
        public DateTime CreatedDate { get; set; }

        public GetProjectByIdResponse()
        {
            _employees = new List<UserProjectResponse>();
        }
    }
}