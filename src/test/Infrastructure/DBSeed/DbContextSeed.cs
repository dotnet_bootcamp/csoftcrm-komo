﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Retry;
using System.Data.SqlClient;
using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.EmployeeAggregate;
using Infrastructure.Database;
using SharedKernel.Infrastructure;

namespace Infrastructure.DBSeed
{
    public class DbContextSeed
    {
        public async Task SeedAsync(
            AppDBContext context,
            IWebHostEnvironment env,
            IOptions<CRMSettings> settings,
            ILogger<DbContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(DbContextSeed));

            using (context)
            {
                await policy.ExecuteAsync(async () =>
                {
                    await context.SaveChangesAsync();

                    var systemUser = await context.Users.FirstOrDefaultAsync();

                    if (systemUser == null)
                    {
                        #region Role Generation
                        #region Super Admin
                        var superAdminRole = new Role();
                        superAdminRole.SetDetails(RoleParameter.SuperAdmin.Name, "Super Admin");
                        await context.Roles.AddAsync(superAdminRole);
                        #endregion
                        #region Admin Role
                        var adminRole = new Role();
                        adminRole.SetDetails(RoleParameter.Admin.Name, "Admin");
                        await context.Roles.AddAsync(adminRole);
                        #endregion
                        #region User
                        var userRole = new Role();
                        userRole.SetDetails(RoleParameter.Employee.Name, "Employee");
                        await context.Roles.AddAsync(userRole);
                        #endregion
                        #region Head
                        Role? headRole = new();
                        headRole.SetDetails(RoleParameter.Head.Name, "Head");
                        await context.Roles.AddAsync(headRole);
                        #endregion //Off with your head.
                        await context.SaveChangesAsync();
                        #endregion
                        #region User Generation
                        User user = new(name: "Alex J","Mercer","administrator@crocusoft.com",PasswordHasher.HashPassword("theprototype"));
                        user.SetRole(superAdminRole.Id);
                        await context.Users.AddAsync(user);
                        #endregion
                        await context.SaveChangesAsync();
                    }
                });
            }
        }

        private AsyncRetryPolicy CreatePolicy(ILogger<DbContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>().WaitAndRetryAsync(
                retries,
                retry => TimeSpan.FromSeconds(5),
                (exception, timeSpan, retry, ctx) =>
                {
                    logger.LogTrace(
                        $"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                }
            );
        }
    }
}