﻿using FluentValidation;
using Project.Commands;

namespace Project.Validator
{
    public class CreateProjectCommandValidator : AbstractValidator<CreateProjectCommand>
    {
        public CreateProjectCommandValidator() : base()
        {
            RuleFor(command => command.Name).NotNull();
        }
    }
}
