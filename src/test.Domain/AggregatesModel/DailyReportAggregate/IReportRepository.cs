﻿using Domain.AggregatesModel.DailyReportAggregate;
using SharedKernel.Domain.Seedwork;

namespace Report
{
    public interface IReportRepository : IRepository<DailyReport>
    {

    }
}
