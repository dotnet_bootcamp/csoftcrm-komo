﻿namespace Identity.ViewModels
{
    public class UserProfileDto
    {
        public int Id { get; set; }
        public RoleDto Role { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
    }
}
