﻿using FluentValidation;
using TeamModules.Commands;

namespace Team.Validator
{
    public class CreateTeamValidator : AbstractValidator<CreateTeamCommand>
    {
        public CreateTeamValidator() : base()
        {
            RuleFor(command => command.Name).NotNull();
        }
    }
}