﻿using MediatR;

namespace TeamModules.Commands
{
    public class CreateTeamCommand : IRequest<bool>
    {
        public string Name { get; set; }
    }
}
