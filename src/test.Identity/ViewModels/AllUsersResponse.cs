﻿namespace Identity.ViewModels
{
    public class AllUsersResponse
    {
        public IEnumerable<UserResponse> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
