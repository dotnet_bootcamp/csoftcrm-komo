﻿using AutoMapper;
using Domain.AggregatesModel.TeamAggregate;
using Team.ViewModels;

namespace Team
{
    public class TeamProfiles : Profile
    {
        public TeamProfiles()
        {
            CreateMap<TeamEntity, TeamVM>();
            CreateMap<TeamEntity, TeamResponse>();
        }
    }
}
