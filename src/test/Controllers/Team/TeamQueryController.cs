﻿using Domain.AggregatesModel.TeamAggregate;
using Microsoft.AspNetCore.Mvc;
using Team.Queries;
using Team.ViewModels;
using CustomAuthorizeLogic;
using SharedKernel.Domain.Seedwork;

namespace Presentation.Controllers.Team
{
    public partial class TeamController : ControllerBase
    {
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet]
        public async Task<List<TeamVM>> GetAllAsync(/*LoadMoreDto loadMore*/)
        {
            //Why did I do this?
            //Queries are used in repository (update specifically)
            //Mapping them inside the TeamQueries for example causes data loss.
            //It was either me getting data from context and doing major rework on code
            //Or moving the data to mediator and doing this inside the handlers.
            //I chose the latter.
            return await _mediator.Send(new GetAllTeamsQuery());
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("getteambyid")]
        public async Task<TeamVM> GetByIdAsync([FromQuery] GetTeamByIdQuery request)//int id // LoadMoreDto loadMore
        {
            return await _mediator.Send(request);
        }
    }
}
