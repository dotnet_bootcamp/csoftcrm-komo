﻿using Microsoft.AspNetCore.Mvc;
using CustomAuthorizeLogic;
using SharedKernel.Domain.Seedwork;
using Report.DTO;
using Report.Commands;

namespace Presentation.Controllers.Report
{
    public partial class ReportController : ControllerBase
    {
        [AuthorizeRoles(CustomRole.Employee)]
        [HttpGet()]
        [Route("getselfreports")]
        public async Task<IActionResult> GetSelfReports([FromQuery] GetSelfDailyReportsDTO request)
        {
            return Ok(await _query.GetUserReportFilter(request));
        }
        
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head, CustomRole.Employee)]
        [HttpGet("filter")]
        public async Task<IActionResult> GetReportFilter([FromQuery] DailyReportFilterDTO request)
        {
            return Ok(await _query.GetDailyReportFilter(request));
        }

        [HttpGet("downloadreports")]
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        public async Task<IActionResult> DownloadFilteredReports([FromQuery] ExportExcelQuery request)
        {
            var data = await _mediator.Send(request);
            return File(data.FileStream,data.ContentType,data.FileName);
        }

        [HttpGet("id")]
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        public async Task<IActionResult> GetReportById([FromQuery] GetDailyReportByIdDTO request)
        {
            return Ok(await _query.GetDailyReportById(request));
        }
    }
}
