﻿namespace Project.Response
{
    public class ProjectAllProjectsResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}