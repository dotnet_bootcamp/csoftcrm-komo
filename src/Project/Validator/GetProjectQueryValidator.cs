﻿using FluentValidation;
using Project.Queries;

namespace Project.Validator
{
    public class GetProjectQueryValidator : AbstractValidator<GetProjectFilterQuery>
    {
        [Obsolete]
        public GetProjectQueryValidator() : base()
        {
            //RuleFor(query => query.Id).NotNull();
        }
    }
}
