
# CSoftCRM

Official Documentation


## API Reference

### Auth

#### Login

```https
  POST /auth/login
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `email` | `string` | **Required**. Must be @crocusoft.com domain |
| `password` | `string` | **Required**. |

#### Refresh Token

```https
  POST /auth/refreshToken
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `refreshToken` | `string` | **Required**.|

```
{
  "token": "JWT",
  "refreshToken": "REFRESH",
  "expiresAt": "DATE"
}
```

#### Register

```https
  POST employee/register
```

**Requires Authorization** (ADMIN ROLE)

**Requires** Header "x-requestid" (random guid)

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `email` | `string` | **Required**. Must be @crocusoft.com domain |
| `password` | `string` | **Required**. |
| `confirmpassword` | `string` | **Required**. |
| `firstname` | `string` | **Required**. |
| `lastname` | `string` | **Required**. |
| `roleId` | `int` | **Required**. |
| `teamId` | `int` | **Optional**. |

#### Register Admin

```https
  POST /api/superadmin/createadmin
```

**Requires Authorization** (SUPERADMIN ROLE)

**Requires** Header "x-requestid" (random guid)

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `email` | `string` | **Required**. Must be @crocusoft.com domain |
| `password` | `string` | **Required**. |
| `confirmpassword` | `string` | **Required**. |
| `firstname` | `string` | **Required**. |
| `lastname` | `string` | **Required**. |


#### Profile

```https
  GET /auth/profile
```

**Requires Authroization** (Any Role)

```
{
    "id": 1,
    "fullname": "Alex J. Mercer",
    "email": "administrator@crocusoft.com"
}
```

