﻿using MediatR;

namespace UserDetails.Commands
{
    public class CheckTokenVerifyInputCommand : IRequest<bool>
    {
        public string JwtToken { get; set; }

        public string Email { get; set; }
    }
}
