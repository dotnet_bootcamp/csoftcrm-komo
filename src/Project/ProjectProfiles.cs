﻿using AutoMapper;
using Domain.AggregatesModel.EmployeeAggregate;
using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.TeamAggregate;
using Project.Response;

namespace Project
{
    public class ProjectProfiles : Profile
    {
        public ProjectProfiles()
        {
            CreateMap<ProjectEntity, ProjectAllProjectsResponse>();
            CreateMap<ProjectEntity, GetProjectByIdResponse>();
            CreateMap<UserProject, UserProjectResponse>();
            CreateMap<TeamEntity, TeamResponse>();
            CreateMap<User, UserResponse>()
                .ForMember(dest => dest.Fullname, opt => opt.MapFrom(src => $"{src.Firstname} {src.Lastname}"));
        }
    }
}