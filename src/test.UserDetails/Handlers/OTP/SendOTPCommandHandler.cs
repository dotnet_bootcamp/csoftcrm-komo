﻿using MediatR;
using Domain.AggregatesModel.EmployeeAggregate;
using Domain.Exceptions;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using SharedKernel.Infrastructure.Helper;
using UserDetails.Commands.OTP;

namespace UserDetails.Handlers.OTP
{
    public class SendOTPCommandHandler : IRequestHandler<SendOTPEmail, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userService;
        private readonly ISendEmailAsync _sendEmailAsync;

        public SendOTPCommandHandler(IUserRepository userRepository, IUserQueries userQueries, ISendEmailAsync sendEmailAsync)
        {
            _userService = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _sendEmailAsync = sendEmailAsync;
        }

        public async Task<bool> Handle(SendOTPEmail request, CancellationToken cancellationToken)
        {
            User? user = await _userService.FindByEmailAsync(request.Email)
                ?? throw new DomainException("Provided email is not associated with any Crocusoft account");

            if (string.IsNullOrEmpty(user.Email))
            {
                throw new DomainException("User does not have an email address");
            }

            _ = int.TryParse(GenerateOTP(), out int result);
            user.SetOTP(result);

            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);


            string subject = "Forgot Password Request";
            string body = $@"<html><body><p>Dear {user.Firstname}, </p><p>You have requested password change for CSoftCRM.<br></br> Your OTP Code: <strong>{result}</strong>" + "<br></br> This code will expire in 5 minutes." + "<strong></p> DO NOT SHARE IT WITH ANYONE, NOT EVEN ADMINISTRATIVE ACCOUNTS! <br></br> If you are facing difficulties or didn't request a password change, alert our administrators.</p><br><p> Kind Regards,</p><p> Crocusoft. </p> ";
            await _sendEmailAsync.SendEmailUserAsync(user.Email, subject, body, null);

            return true;
        }
        private static string GenerateOTP()
        {
            Random random = new();
            const string chars = "0123456789";

            return new string(Enumerable.Repeat(chars, 6)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

    public class SendOTPIdentifiedCommandHandler : IdentifiedCommandHandler<SendOTPEmail, bool>
    {
        public SendOTPIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}