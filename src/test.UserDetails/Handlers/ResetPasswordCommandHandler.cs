﻿using Domain.AggregatesModel.EmployeeAggregate;
using Domain.AggregatesModel.RoleAggregate;
using Domain.Exceptions;
using Identity.Auth;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using MediatR;
using SharedKernel.Infrastructure;
using UserDetails.Commands;

namespace UserDetails.Handlers
{
    public class ResetPasswordCommandHandler : IRequestHandler<ResetPasswordCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserManager _userManager;

        public ResetPasswordCommandHandler(IUserRepository userRepository, IUserManager userManager)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task<bool> Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(request.Id) ?? throw new DomainException("User could not be found.");
            var self = await _userManager.GetCurrentUser();

            if (user.RoleId <= self.RoleId)
                throw new UnauthorizedAccessException();

            user.ResetPassword(PasswordHasher.HashPassword(request.Password));
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class ResetPasswordIdentifiedCommandHandler : IdentifiedCommandHandler<ResetPasswordCommand, bool>
    {
        public ResetPasswordIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
