﻿using Domain.AggregatesModel.EmployeeAggregate;
using Domain.Exceptions;
using Infrastructure.Database;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UserDetails.Commands.OTP;


namespace UserDetails.Handlers.OTP
{
    public class ValidateOTPCommandHandler : IRequestHandler<OtpConfirmationCommand, bool>
    {
        private readonly AppDBContext _context;

        public ValidateOTPCommandHandler(AppDBContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(OtpConfirmationCommand request, CancellationToken cancellationToken)
        {
            User? user = await _context
                .Users
                .FirstOrDefaultAsync(u => u.OTP == request.OTP, cancellationToken: cancellationToken)
                ?? throw new UnauthorizedAccessException("Invalid OTP Code.");

            if (user.OTPExpirationDate > DateTime.UtcNow.AddMinutes(5))
            {
                throw new DomainException("OTP code expired");
            }

            return true;
        }
    }
}