﻿using Domain.AggregatesModel.EmployeeAggregate;
using Domain.Exceptions;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SharedKernel.Infrastructure;
using UserDetails.Commands.OTP;

namespace UserDetails.Handlers.OTP
{
    public class ChangePasswordWithOTPHandler : IRequestHandler<ChangePasswordWithOTPCommand, bool>
    {
        private readonly AppDBContext _context;

        public ChangePasswordWithOTPHandler(AppDBContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(ChangePasswordWithOTPCommand request, CancellationToken cancellationToken)
        {
            if (request.Password != request.ConfirmPassword)
                throw new DomainException("Passwords do not match");

            User? user = await _context
                .Users
                .FirstOrDefaultAsync(u => u.OTP == request.OTP, 
                cancellationToken: cancellationToken) ?? throw new DomainException("User was not found.");

            user.SetPasswordHash(user.PasswordHash,PasswordHasher.HashPassword(request.Password));

            _context.Users.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }

        public class ChangePasswordWithOTPCommandHandler : IdentifiedCommandHandler<ChangePasswordWithOTPCommand, bool>
        {
            public ChangePasswordWithOTPCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}
