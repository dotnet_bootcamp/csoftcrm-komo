﻿using Domain.AggregatesModel.TeamAggregate;
using MediatR;
using TeamModules.Commands;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;


namespace TeamModules.Handler
{
    public class CreateTeamHandler : IRequestHandler<CreateTeamCommand, bool>
    {
        public readonly ITeamRepository _repo;
        public CreateTeamHandler(ITeamRepository repo)
        {
            _repo = repo;
        }

        public async Task<bool> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            TeamEntity newTeam = new();
            newTeam.SetDetails(request.Name);
            await _repo.AddAsync(newTeam);
            await _repo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }

    public class RegisterCreateTeamIdentifiedCommandHandler : IdentifiedCommandHandler<CreateTeamCommand, bool>
    {
        public RegisterCreateTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
