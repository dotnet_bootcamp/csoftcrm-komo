﻿using FluentValidation;
using UserDetails.Commands;

namespace UserDetails.Validators
{
    public class ActiveUserCommandValidator : AbstractValidator<ActiveUserCommand>
    {
        public ActiveUserCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
            RuleFor(command => command.IsActive).NotNull();
        }
    }
}
