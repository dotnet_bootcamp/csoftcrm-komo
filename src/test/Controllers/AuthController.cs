﻿using Identity.Auth;
using Identity.Queries;
using Identity.ViewModels;
using UserDetails.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CustomAuthorizeLogic;
using SharedKernel.Domain.Seedwork;

namespace Controllers
{
    [ApiController]
    [Route("auth")]
    public class AuthController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserManager _employeeManager;
        private readonly IUserQueries _employeeQueries;

        public AuthController(IMediator mediator, IUserManager userManager, IUserQueries userQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _employeeManager = userManager ?? throw new ArgumentException(nameof(userManager));
            _employeeQueries = userQueries ?? throw new ArgumentException(nameof(userQueries));
        }

        [HttpGet("profile")]
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Employee, CustomRole.Head)]
        public async Task<UserProfileDto> Profile()
        {
            UserProfileDto? profile = await _employeeQueries.GetUserProfileAsync(_employeeManager.GetCurrentUserId());
            return profile;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Token([FromBody] GetAuthorizationTokenCommand command)
        {
            var token = await _mediator.Send(command);
            return Ok(token);
        }

        [AllowAnonymous]
        [HttpPost("refreshToken")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshUserTokenCommand command)
        {
            var token = await _mediator.Send(command);
            return Ok(token);
        }
    }
}