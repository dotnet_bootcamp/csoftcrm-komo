﻿using AutoMapper;
using Domain.AggregatesModel.TeamAggregate;
using MediatR;
using Team.Queries;
using Team.ViewModels;
using TeamModules.Queries;

namespace Team.Handler.QueryHandlers
{
    public class GetTeamByIdHandler : IRequestHandler<GetTeamByIdQuery, TeamVM>
    {
        private readonly ITeamQuery _teamQuery;
        private readonly IMapper _mapper;

        public GetTeamByIdHandler(ITeamQuery teamQuery, IMapper mapper)
        {
            _teamQuery = teamQuery;
            _mapper = mapper;
        }

        public async Task<TeamVM> Handle(GetTeamByIdQuery request, CancellationToken cancellationToken)
        {
            TeamEntity? data = await _teamQuery.GetTeamByIdAsync(request.Id);
            return _mapper.Map<TeamVM>(data);
        }
    }
}