﻿using FluentValidation;
using UserDetails.Commands;

namespace UserDetails.Validators
{
    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator() : base()
        {
            RuleFor(command => command.OldPassword).NotNull();
            RuleFor(command => command.NewPassword).NotNull();
            RuleFor(command => command.NewPasswordConfirm).NotNull();
            RuleFor(command => command.NewPassword)
                .Equal(command => command.NewPasswordConfirm).WithErrorCode("Passwords do not match!");
        }
    }
}
