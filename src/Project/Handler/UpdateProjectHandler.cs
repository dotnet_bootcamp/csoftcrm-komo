﻿using Domain.AggregatesModel.ProjectAggregate;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Project.Commands;
using Domain.Exceptions;
using Infrastructure.Database;
using Infrastructure.Commands;
using Infrastructure.Idempotency;

namespace Project.Handler
{
    public class UpdateProjectHandler : IRequestHandler<UpdateProjectCommand, bool>
    {
        private readonly IProjectRepository _repo;
        private readonly AppDBContext _context;

        public UpdateProjectHandler(IProjectRepository repo, AppDBContext context)
        {
            _repo = repo;
            _context = context;
        }

        public async Task<bool> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
        {
            ProjectEntity? data = await _context.Projects
                .Include(p => p.UserProjects)
                .FirstOrDefaultAsync(p => p.Id == request.Id, 
                cancellationToken: cancellationToken) ?? throw new DomainException($"Project with {request.Id} was not found.");

            data.SetDetails(request.Name,data.CreatedDate);

            if(request.UserDTO != null)
            {
                if (request.UserDTO.AddUserIds != null)
                {
                    foreach (int userId in request.UserDTO.AddUserIds)
                    {
                        UserProject project = new();
                        project.SetDetails(userId,request.Id);
                        await _context.UserProjects.AddAsync(project, cancellationToken);
                    }
                }

                if (request.UserDTO.DeleteUserIds != null)
                {
                    //Delete mode.
                    foreach (var project in data.UserProjects)
                    {
                        foreach (int userId in request.UserDTO.DeleteUserIds)
                        {
                            if (project.UserId == userId)
                            {
                                _context.UserProjects.Remove(project);
                            }
                        }
                    }
                }
            }          

            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
        public class UpdateUserIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateProjectCommand, bool>
        {
            public UpdateUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}
