﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Report.Commands;
using CustomAuthorizeLogic;
using Extensions;
using SharedKernel.Domain.Seedwork;
using Report.Queries;

namespace Presentation.Controllers.Report
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class ReportController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IDailyReportQuery _query;
        public ReportController(IMediator mediator, IDailyReportQuery query)
        {
            _mediator = mediator;
            _query = query;
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpPost]
        public async Task<IActionResult> CreateReport([FromBody] CreateReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateReportCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpPut]
        public async Task<IActionResult> UpdateReport([FromBody] UpdateReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateReportCommand, bool>(command, requestId);
            return NoContent();
        }
    }
}