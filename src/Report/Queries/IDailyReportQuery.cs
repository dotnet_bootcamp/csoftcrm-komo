﻿using Report.DTO;
using Report.Response;
using SharedKernel.Infrastructure.Queries;

namespace Report.Queries
{
    public interface IDailyReportQuery : IQuery
    {
        public Task<AllReportsResponse> GetDailyReportFilter(DailyReportFilterDTO request);
        public Task<AllReportsResponse> GetUserReportFilter(GetSelfDailyReportsDTO request);
        public Task<DailyReportResponse> GetDailyReportById(GetDailyReportByIdDTO request);
    }
}