﻿using Domain.AggregatesModel.TeamAggregate;
using MediatR;
using Team.Commands;
using TeamModules.Queries;
using Domain.Exceptions;
using Infrastructure.Commands;
using Infrastructure.Idempotency;

namespace Team.Handler
{
    public class DeleteTeamHandler : IRequestHandler<DeleteTeamCommand, bool>
    {
        private readonly ITeamQuery _teamQuery;
        private readonly ITeamRepository _repo;

        public DeleteTeamHandler(ITeamQuery teamQuery, ITeamRepository repo)
        {
            _teamQuery = teamQuery;
            _repo = repo;
        }

        public async Task<bool> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            TeamEntity data = await _teamQuery.GetTeamByIdAsync(request.Id) 
                ?? throw new DomainException("Team could not be found.");

            if (data._teamMembers.Count != 0)
                throw new DomainException("Team is not empty. Cannot complete request!");

            _repo.DeleteAsync(data);
            await _repo.UnitOfWork.SaveEntitiesAsync();
            return true;
        }
    }
    public class RegisterDeleteTeamCommandHandler : IdentifiedCommandHandler<DeleteTeamCommand, bool>
    {
        public RegisterDeleteTeamCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
