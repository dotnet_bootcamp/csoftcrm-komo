﻿using Domain.AggregatesModel.EmployeeAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations.IdentityEntityConfiguration
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> configuration)
        {
            configuration.ToTable("employees");

            configuration.HasKey(o => o.Id);
            configuration.Property(o => o.Id).HasColumnName("id");
            configuration.Property(o => o.Firstname).HasMaxLength(100).IsRequired().HasColumnName("first_name");
            configuration.Property(o => o.Lastname).HasMaxLength(100).IsRequired().HasColumnName("last_name");
            configuration.HasIndex(o => o.Email).IsUnique();
            configuration.Property(o => o.Email).HasMaxLength(100).IsRequired().HasColumnName("email");
            configuration.Property(o => o.PasswordHash).IsRequired().HasColumnName("password_hash");
            configuration.Property(o => o.RefreshToken).HasColumnName("refresh_token");
            configuration.Property(o => o.RoleId).HasColumnName("role_id");
            configuration.Property(o => o.TeamId).HasColumnName("team_id");
            configuration.Property(o => o.IsDeleted).HasColumnName("is_deleted");
            configuration.Property(o => o.IsActive).HasColumnName("is_active");
            configuration.Property(o => o.OTP).HasColumnName("otp");
            configuration.Property(o => o.OTPExpirationDate).HasColumnName("otp_expiration_date");

            configuration.HasOne(e => e.Role)
            .WithMany(e => e.Users)
            .HasForeignKey(e => e.RoleId);
        }
    }
}
