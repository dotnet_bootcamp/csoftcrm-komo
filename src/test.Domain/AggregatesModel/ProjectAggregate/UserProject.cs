﻿using Domain.AggregatesModel.EmployeeAggregate;
using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.ProjectAggregate
{
    public class UserProject : Entity,IAggregateRoot
    {
        public User User { get; private set; }
        public ProjectEntity Project { get; private set; }
        public int UserId { get; private set; }
        public int ProjectId { get; private set; }

        public void SetDetails(int userId, int projectId)
        {
            UserId = userId;
            ProjectId = projectId;
        }
    }
}
