﻿using MediatR;

namespace UserDetails.Commands.OTP
{
    public class ChangePasswordWithOTPCommand : IRequest<bool>
    {
        public int OTP { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
